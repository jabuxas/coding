import math


print("You can choose between sum (+), subtraction (-), multiplication (*) or division (/)")


def calc():
    ans = input("What do you want to do? ")
    x = input("Select your first number: ")
    y = input("Select your second number: ")
    try:
        float(x)
        float(y)
    except:
        print("Input not valid.")
    x = float(x)
    y = float(y)
    if ans == "+":
        print(x, "plus", y, "is:", float(x) + float(y))
    if ans == "-":
        print(x, "minus", y, "is:", float(x) - float(y))
    if ans == "*":
        z = x * y
        print(x, "times", y, "is:", round(z, 2))
    if ans == "/":
        z = x / y
        print(x, "divided by", y, "is:", round(z, 2))
    return


calc()


def loop(target):
    if target.lower() == "yes":
        calc()
        loop_ans2 = input("Do you want to do another one yet again? ")
        loop(loop_ans2)
        return loop(loop_ans2)
    else:
        quit()


loop_ans = input("Do you want to do another calculation? ")
loop(loop_ans)
