name = "Lucas"
-- "\"" puts quotes on text (counting spaces)
-- # counts the number of letters in a string.
io.write(name, " has ", "\"", #name, "\"", " letters.") -- io.write should be the same as print
--[[
This is a multiline comment
Need two dashes + two opening brackets
In case you want to uncomment a line of code commented out via the brackets method, only add
a third hiphen to the start of the commenting
]]
age = 16
print("\nLucas is " .. age .. " years old.") -- .. = concatenation 
