--[[
	nil (nothing)
	number 1 1.3 0.1 44
	string 'hello' "hello world"
	boolean true false
	tables (arrays, loops)
	local creates a variable
]]

local name = "Lucas"
local sentence = name .. " is a very nice boy."

print(sentence)

local name = "João"

print(sentence)

local BigSentence = [[
Death, such a sweet fragrance!
Kill, kill!
Let the flower of life bloom brightly in the blood.]]
print (BigSentence)

local epic = true

--[[ creating global scope variables is very easy.
all variables that don't start with the "local" 
sign, are global. i.e:
g = 20  (global)
local g = 10 (local)

but, you can also make a variable global by starting it with _G.

_G.Hello = "Hi!"
print(Hello) would result in Hi!
]]
print(
	type(name),
	type(sentence),
	type(epic),
	type(BigSentence)
)


